package am.aybgim.bootreact;

import org.springframework.web.bind.annotation.PathVariable;
import org.springframework.web.bind.annotation.RequestMapping;
import org.springframework.web.bind.annotation.RequestParam;
import org.springframework.web.bind.annotation.RestController;

import java.util.HashMap;
import java.util.Map;

@RestController
public class DataController {

    @RequestMapping(value = "/primary", produces = "application/json")
    public Map<String, String> primaryValues() {
        return new HashMap<String, String>() {
            {
                put("p1", "P 1");
                put("p2", "P 2");
            }
        };
    }

    @RequestMapping(value = "/secondary/{primaryId}", produces = "application/json")
    public Map<String, String> secondaryValues(@PathVariable String primaryId) {
        switch (primaryId) {
            case "p1":
                return new HashMap<String, String>() {
                    {
                        put("s1", "P 1: S 1");
                        put("s2", "P 1: S 2");
                    }
                };
            case "p2":
                return new HashMap<String, String>() {
                    {
                        put("s1", "P 2: S 1");
                        put("s2", "P 2: S 2");
                    }
                };
            default: {
                System.err.println("Unknown type:" + primaryId);
                throw new IllegalArgumentException();
            }
        }
    }
}
